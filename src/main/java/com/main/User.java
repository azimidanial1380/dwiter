package com.main;

import java.util.ArrayList;
import java.util.List;

public class User {
    protected User() {}
    protected User(String userId, String userPassword) {
        this.userId = userId ;
        this.userPassword = userPassword ;
        setUserTimeline(new Timeline(getUserId(),userTimelineTweets));
        setUserInfoPage(new UserInfoPage(getUserId(),userPersonalTweets)) ;
    }

    private String userId ;
    private String userPassword ;
    private Timeline userTimeline;
    private UserInfoPage userInfoPage ;
    private List<User> userFollowings = new ArrayList<>();
    private List<User> userFollowers= new ArrayList<>();

    public List<User> getUserFollowers() {
        return userFollowers;
    }

    public void addUserFollowers(User user) {
        this.userFollowers.add(user);
    }

    List<Tweet> userTimelineTweets = new ArrayList<>();
    List<Tweet> userPersonalTweets = new ArrayList<>();

    protected Timeline getUserTimeline() {
        return userTimeline;
    }

    protected void setUserTimeline(Timeline userTimeline) {
        this.userTimeline = userTimeline;
    }
    protected List<String> timelineList(){
        return getUserTimeline().display() ;
    }


    protected UserInfoPage getUserInfoPage() {
        return userInfoPage;
    }

    protected void setUserInfoPage(UserInfoPage userInfoPage) {
        this.userInfoPage = userInfoPage;
    }
    protected List<String> userInfoPageList(){
        return getUserInfoPage().display() ;
    }


    protected String getUserId() {
        return userId;
    }
    protected String getUserPassword() {
        return userPassword;
    }

    protected String tweet(String tweet) {
        save(tweet);
        return tweet;
    }

    void save(String status) {
        Tweet tweet = new Tweet(status, getUserId() );
        userTimelineTweets.add(tweet);
        userPersonalTweets.add(tweet) ;
    }
    void follow(User user){
        if(!userFollowings.contains(user)){
            userFollowings.add(user) ;
        }
        else {
           System.out.printf("You followed %s before! \n" , user.getUserId() );
        }
        userTimeline.add(user.userTimelineTweets);
    }
    void unfollow(User user){
        if(userFollowings.contains(user)){
            userFollowings.remove(user) ;
        }
        else {
            System.out.printf("You are not following %s before! \n" , user.getUserId() );
        }
        userTimeline.add(user.userTimelineTweets);
    }
    List<String> followings(){
        List<String> FollowingsUsername = new ArrayList<>();
        for(int i = 0 ; i < userFollowings.size() ; i++){
            FollowingsUsername.add(userFollowings.get(i).getUserId()) ;
        }
        return FollowingsUsername ;
    }

    List<String> followers(){
        List<String> FollowersUsername = new ArrayList<>();
        for(int i = 0 ; i < userFollowers.size() ; i++){
            FollowersUsername.add(userFollowers.get(i).getUserId()) ;
        }
        return FollowersUsername ;
    }

}
