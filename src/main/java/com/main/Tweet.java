package com.main;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Tweet {

    Tweet(String status, String username) {
        setTweetText(status) ;
        this.username = username;
        this.timestamp = time();
    }

    // fields and belongings:
    String username;
    String timestamp;
    private String tweetText ;
    private final int TWEET_MAX_LENGTH = 140 ;    /*    can be static */
    private TweetIdGenerator idGen = new TweetIdGenerator() ;
    private final String tweetId = idGen.idMaker() ;
    private int likes = 0;

    public String getTweetId() {
        return tweetId;
    }

    protected int getLikes() {
        return likes;
    }

    protected void addLikes() {
        this.likes++ ;
    }

    String time() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    protected String checkLength(String text){
        String modifiableText = text;
        if( text.length() > TWEET_MAX_LENGTH){        /*    Just delete from char number 141 ! */
            modifiableText = text.substring(0,TWEET_MAX_LENGTH) ;
        }
        return modifiableText ;
    }

    protected String getTweetText() {
        return tweetText;
    }

    protected void setTweetText(String tweetText) {
        this.tweetText = checkLength(tweetText) ;
    }

}
