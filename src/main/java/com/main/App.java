package com.main;

import java.util.*;
import static javafx.application.Platform.exit;

public class App {

    List<User> users = new ArrayList<>() ;
    User currentUser ;
    Scanner reader = new Scanner(System.in).useDelimiter("\\n") ;

    public static void main(String[] args) {
        new App().menu();
    }

    public void menu(){
        printMenu() ;
        Scanner main = new Scanner(System.in);
        int chooseAction = main.nextInt() ;
        clearScreen() ;
            switch (chooseAction) {
                case 1:
                    signIn();
                    userCommands(currentUser.getUserId()) ;
                    break;
                case 2:
                    signUp();
                    break;
                case 3:
                    Exit();
                    return ;
                default:
                    System.out.println("Wrong Input! \nTry again! ");
                    break;
            }
        menu() ;
    }

    public void border(){
        System.out.println("___________________________________________________________________");
    }

    public void signIn(){
        border() ;
        System.out.println("Sign In Page               (press * for going back to MENU)");
        System.out.println("Username: ");
        String UserId = reader.next() ;
        if(UserId.equals("*")){
            menu();
            return;     // stop continuance of the last function(s) (that ended up here!)
        }
        System.out.println("Password:");
        String userPassword = reader.next() ;
        if(userPassword.equals("*")){
            menu();
            return ;
        }
        // level 1: check ID existence
        User userExistence = searchUsername(UserId) ;
        if(userExistence==null){
            System.out.println("Sorry! Username does not exist!");
            System.out.println("press 0 to see the Menu");
            System.out.println("press any other key to refresh Sign In Page...");
            String cmd = reader.next() ;
            if(cmd.equals("*")){
                menu();
                return;     // stop continuance of the last function(s) (that ended up here!)
            }else{
                signIn();                  // recursion
                return ;    // stop continuance of the last function(s) (that ended up here!)
            }
        }
        // level 2: check password integrity
        boolean passwordCredit = checkPassword(userExistence,userPassword);
        if(passwordCredit){
            currentUser = userExistence ;         // Finally you can set the current user with your ID
        }
        else {
            System.out.println("Sorry! Wrong Password!");
            System.out.println("Try again...");
            signIn(userExistence) ;
        }
    }

    public void signIn(User Need_Password){     // Used for both from sign in or from Wrong pass
        // If someday wanna change each BE CAREFULL about the other one!!!
        border() ;
        System.out.println("Sign In Page               (press * for going back to MENU)");
        String chose_username = Need_Password.getUserId() ;
        System.out.printf("Username: %s\n",chose_username);
        System.out.println("Password:");
        String userPassword = reader.next() ;
        if(userPassword.equals("*")){
            menu();
            return ;
        }
        boolean passwordCredit = checkPassword(Need_Password,userPassword);
        if(passwordCredit){
            currentUser = Need_Password ;       // Finally you can set the current user with your ID
        }
        else{
            System.out.println("Sorry! Wrong Password!");
            System.out.println("Try again...");
            signIn(Need_Password) ;
        }
    }

    public void signUp(){
        border() ;
        System.out.println("Sign Up Page               (press * for going back to MENU)");
        System.out.println("Username: ");
        String newUserID = reader.next() ; ///  for search in next lines change it to object from string or...
        if(newUserID.equals("*")){
            menu();
            return ;
        }
        User checkInSearch = searchUsername(newUserID) ;

        if(checkInSearch==null){     //  why (==) worked (instead of .equals) when tested??
            System.out.println("Password:");
            String newUserPassword = reader.next() ;
            if(newUserPassword.equals("*")){
                menu();
                return ;
            }
            User newUser = new User(newUserID,newUserPassword) ;
            users.add(newUser) ;
            System.out.println("DONE! Your Account is ready to use!");
            System.out.println("Options:" +
                    "\n (1)SignIn (from the last created Account)" +
                    "\n (2)Create another Account" +
                    "\n Press any other key to Go back to Menu...");
            String options = reader.next();
            if(options.equals("*")){
                menu();
                return ;
            }
            switch (options){
                case "1" :
                    // ??? ***    currentUser = newUser ;     Only IF I want to pre_Sign the User!
                    signIn(newUser);     ///     Can be Danger in case of bad  signIn Implementations
                    userCommands(currentUser.getUserId());
                    /* go to signIn function for after setting currentUser ACTIONS!
                     * check for when we come back to this function !! how to end this f*/
                    break;
                case "2" :
                    signUp();                 // might be dangerous recursion !!!
                    break;
            }

        }
        else{
            System.out.print("SORRY!\n Username is token!\n");
            System.out.printf("Are you %s ? If Yes Enter Number 0 to Sign In!" +
                    "\nIf NOT Enter any other key to choose another Username...",checkInSearch.getUserId()) ;
            String SignInUp = reader.next();
            if(SignInUp.equals("*")){
                menu();
                return ;
            }
            if(SignInUp.equals("0")){
                signIn(checkInSearch);         ///     Can be Danger in case of bad  signIn Implementations
                return;
            }else{
                signUp();                 // recursion
            }
        }

    }

    private void userCommands(String user) {
        border() ;
        System.out.printf("Hello %s!\n",user);
        while(true){
            border() ;
            System.out.println("Enter a command: ");
            System.out.println("tweet");
            System.out.println("timeline");
            System.out.println("my profile");   // how?? to ignore case or space   *********\
            System.out.println("profile");
            System.out.println("follow");
            System.out.println("unfollow");
            System.out.println("followings");
            System.out.println("followers");
            System.out.println("sign out");
            border() ;
            String option = reader.next().toLowerCase(); 
            switch (option){
                case "tweet":
                    border() ;
                    tweet();
                    break;
                case "timeline":
                    border() ;
                    System.out.println("username: ");
                    User timelineUser = searchUsername(reader.next()) ;
                    timeline(timelineUser);
                    border() ;
                    break;
                case "my profile":
                    border() ;
                    myProfile();
                    border() ;
                    break;
                case "profile":
                    border() ;
                    System.out.println("username: ");
                    User profileUser = searchUsername(reader.next()) ;
                    profile(profileUser);
                    border() ;
                    break;
                case "follow":
                    border() ;
                    follow();
                    border() ;
                    break;
                case "unfollow":
                    border() ;
                    unfollow();
                    border() ;
                    break;
                case "followings":
                    border() ;
                    followings() ;
                    border() ;
                    break;
                case "followers":
                    border() ;
                    followers() ;
                    border() ;
                    break;
                case "sign out":
                    border() ;
                    if(signOut() ){
                        return;                 //       only way out of the loop !
                    }
                    break ;
                default:
                    System.out.println("command doesn't exsist! check the below list:");
                    break ;
            }
        }
    }

    private void tweet(){
        System.out.printf("What's on your mind %s ? \n " , currentUser.getUserId() );
        String status = reader.next();
        status = currentUser.tweet(status) ;
        String tweet = String.format("@%s said \"%s\" ", currentUser.getUserId(), status);
        System.out.println(tweet);

    }

    private void timeline(User user){
        if(!user.timelineList().isEmpty()){
            user.timelineList().forEach(System.out::println);
        }
        else{
            System.out.printf("%s's TimeLine is Empty! \n" , user.getUserId() );
        }
        likeTweet(user) ;
    }
    private void likeTweet(User user){
        System.out.println("write ((like)) to like a tweet (anything else to exit this page): ");
        String like = reader.next() ;
        if(like.equalsIgnoreCase("like")){
            System.out.println("Enter a tweetID: ");
            String tweetId = reader.next() ;
            for(int i = 0 ; i < user.getUserTimeline().getTweets().size() ; i++ ){
                if(user.getUserTimeline().getTweets().get(i).getTweetId().equals(tweetId)){
                    user.getUserTimeline().getTweets().get(i).addLikes();
                    break ;
                }
            }
            System.out.println("done! thanks!");
        }
    }
    private void myProfile(){
        if(!currentUser.userInfoPageList().isEmpty()){
            currentUser.userInfoPageList().forEach(System.out::println);
        }
        else{
            System.out.println("You didn't post any tweets yet!");
        }
    }
    private void profile(User user){
        if(!user.userInfoPageList().isEmpty()){
            user.userInfoPageList().forEach(System.out::println);
        }
        else{
            System.out.printf("%s's has no tweet! \n" , user.getUserId() );
        }
        likeTweet(user) ;
    }

    private void follow(){
        System.out.println("please enter a Username: ");
        String username = reader.next() ;
        if(searchUsername(username)!=null){                        //      change it to try and catch !!!
            for (int i = 0 ; i < users.size() ; i++) {
                if(users.get(i).getUserId().equals(username)){            //   how to get to each uesrname!
                    currentUser.follow(users.get(i));
                    System.out.printf("you have successfully followed %s \n" ,users.get(i).getUserId() );
                    users.get(i).addUserFollowers(currentUser);
                    break ;
                }
            }
        }
        else {
            System.out.println("Username doesn't exsist! try again!");
        }
    }
    private void unfollow(){
        System.out.println("please enter a Username: ");
        String username = reader.next() ;
        if(searchUsername(username)!=null){             //      change it to try and catch !!!
            for (int i = 0 ; i < users.size() ; i++) {
                if(users.get(i).getUserId().equals(username)){    //   how to get to each uesrname!
                    currentUser.unfollow(users.get(i));
                    System.out.printf("you have successfully unfollowed %s \n" ,users.get(i).getUserId() );
                    break ;
                }
            }
        }
        else {
            System.out.println("Username doesn't exsist! try again!");
        }
    }
    private void followings(){
        if(!currentUser.followings().isEmpty()){
            currentUser.followings().forEach(System.out::println);
        }
        else {
            System.out.println("you haven't followed anybody yet!");
        }
    }
    private void followers(){
        if(!currentUser.followers().isEmpty()){
            currentUser.followers().forEach(System.out::println);
        }
        else {
            System.out.println("nobody have followed you yet!");
        }
    }

    private boolean signOut(){
        System.out.println("Are you sure? (Y/N)");
        String response = reader.next() ;
        if(response.equalsIgnoreCase("Y")){
        return true ;                         //  is this right?
        }
        return false ;
    }

    private User searchUsername(String username) {
        return users.stream()
                .filter(user -> user.getUserId().equals(username))
                .findFirst().orElse(null);           //     Not a good code for Optional!(so other ways to search??)
    }

    private boolean checkPassword(User user,String password_Candidate) {      // can use a boolean function
        return user.getUserPassword().equals(password_Candidate) ;
    }

    private void printMenu(){
        border() ;
        System.out.println("Wellcome to Dwiter!  \n");
        System.out.println("Enter the related number:");
        System.out.println("(((  MENU  ))) ");
        System.out.println("(1) Sign In ");
        System.out.println("(2) Sign Up ");
        System.out.println("(3) Exit ");
        border() ;
    }

    private void clearScreen() {
       /* for(int clear = 0; clear < 84; clear++)
        {
            System.out.println("\b") ;
        }*/
    }
    public void Exit(){
        exit();
    }
}
