package com.main;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


class Timeline {

    private final String username;

    protected List<Tweet> getTweets() {
        return tweets;
    }

    private List<Tweet> tweets;

    Timeline(String username, List<Tweet> tweets) {
        this.username = username;
        this.tweets = tweets;
    }

    protected List<String> display() {
        return tweets.stream()
                .sorted((tweet1, tweet2) -> tweet1.timestamp
                        .compareTo(tweet2.timestamp))
                .map(tweet -> format(tweet.getTweetText(), tweet.username, tweet.timestamp ,
                        tweet.getLikes() , tweet.getTweetId() ))
                .collect(Collectors.toList());
    }

    protected String format(String status, String username, String timestamp , int likes , String id) {
        return String.format("((((@%s said \"%s\" at  %s likes: %d \n    tweet ID : %s ))))"
                , username, status ,timestamp , likes , id );
    }


    protected void add(List<Tweet> otherTweets) {
        otherTweets.forEach(tweet -> tweets.add(tweet));
    }
}
